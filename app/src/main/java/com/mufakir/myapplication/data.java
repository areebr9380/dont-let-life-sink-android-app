package com.mufakir.myapplication;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class data extends AppCompatActivity {
    private TextView tv;
    TableLayout ll;
    TableRow tr;
    String litreArr[];
    TextView scr,scr1;
    String dateArr[];
    TextView today;
    private RequestQueue mQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);
        today=(TextView)findViewById(R.id.today);
        int b;
        ll= (TableLayout) findViewById(R.id.tl);
        for(b=1;b<32;b++){
            tr=new TableRow(this);
            scr=new TextView(this);
            scr1=new TextView(this);
            scr1.setTextSize(15);
            scr1.setGravity(Gravity.CENTER);
            scr1.setPadding(0, 10, 60, 0);
            scr.setTextSize(15);
            scr.setGravity(Gravity.CENTER);
            scr.setText(String.valueOf(b)+"Date");
            scr1.setText(String.valueOf(b)+" Litres");
            scr.setTypeface(null, Typeface.BOLD);
            scr.setPadding(200,100,100,50);
            scr1.setTypeface(null, Typeface.BOLD);
            scr1.setPadding(300,100,100,100);

            tr.addView(scr);
            tr.addView(scr1);

            ll.addView(tr);
        }
        today.setText("5409 Litres");
//        tv=(TextView)findViewById(R.id.tv);
//        mQueue= Volley.newRequestQueue(this);
//        jsonParse();
    }
    private void jsonParse(){
        ll= (TableLayout) findViewById(R.id.tl);

        String url="http://dontletlifesink.pk/site_files/android_webService.php";
        final TableRow tr=new TableRow(this);
        JsonObjectRequest request =new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray=response.getJSONArray("movie");



                    for(int i=0;i<jsonArray.length();i++){

                        JSONObject movie=jsonArray.getJSONObject(i);
                        String date=movie.getString("date");
//                        tv.setText(String.valueOf(jsonArray.length()));
//                        int total=Integer.parseInt(tv.getText().toString());


                        String ltr=movie.getString("volume");
                        displayTable(date,ltr+" Litres");
//                        tv.append(" The Release year is "+date+" Liter consumed is "+ltr+"\n\n");



                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(),"Error in results",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_SHORT).show();

            }
        });

        mQueue.add(request);


    }
    public void displayTable(String date,String ltr){
        tr=new TableRow(this);
        scr=new TextView(this);
        scr1=new TextView(this);
        scr1.setTextSize(15);
        scr1.setGravity(Gravity.CENTER);
        scr1.setPadding(0, 10, 60, 0);
        scr.setTextSize(15);
        scr.setGravity(Gravity.CENTER);
        scr.setText(date);
        scr1.setText(ltr);
        scr.setTypeface(null, Typeface.BOLD);
        scr.setPadding(200,100,100,50);
        scr1.setTypeface(null, Typeface.BOLD);
        scr1.setPadding(300,100,100,100);

        tr.addView(scr);
        tr.addView(scr1);

        ll.addView(tr);
        Date d = new Date();
        String lessDate="";
        CharSequence sDate  = DateFormat.format("d", d.getTime());
        if(Integer.parseInt(String.valueOf(sDate))<10){
            lessDate="0";
        }
        String lessMonth="";

        CharSequence sMonth  = DateFormat.format("M", d.getTime());
        if(Integer.parseInt(String.valueOf(sMonth))<10){
            lessMonth="0";
        }
        CharSequence sYear  = DateFormat.format("yyyy", d.getTime());
        String makeTodayDate=String.valueOf(sYear)+"-"+""+lessMonth+String.valueOf(sMonth)+"-"+lessDate+""+String.valueOf(sDate);

        if(date.equals(makeTodayDate)){
            today.setText(""+ltr);
        }
    }

}
